#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<b;i++)
#define rrep(i,a,b) for(int i=a;i>=b;i--)
#define ll long long
#define ull unsigned ll
#define take(n) int n;cin>>n
#define mod 1000000007
#define pii pair<int,int>
#define v(i) vector<i>
#define mp(a,b) make_pair(a,b)
#define pb(a) push_back(a)
#define pp pop_back()
#define array(n,name) ll int *name=new ll int[n]
#define takearray(n,name) rep(i,0,n) cin>>name[i];
#define printarray(n,nums) rep(i,0,n) cout<<nums[i]<<" ";
#define Zubin ios::sync_with_stdio(false);
#define Shah cin.tie(NULL);cout.tie(0);
#define nl cout<<endl;
using namespace std;

int xcor[4]={-1,0,0,+1};
int ycor[4]={0,-1,1,0};

ull int power(ull n,int x){
    if(x==0) return 1;
    return n * power(n,x-1);
}



string divideGetRemainder(string quotient,string divisor){

    string remainder;
    do{
        remainder = "";
        int i;
        for(i=0;i<divisor.size();i++){
            if(quotient[i]!=divisor[i]){
                remainder+='1';
            }
            else{
                if(remainder.size()){
                    remainder+='0';
                }
            }
        }
        while(i<quotient.size()) remainder.push_back(quotient[i++]);
        quotient = remainder;
    }while(remainder.size()>=divisor.size());

    while(remainder.size()<divisor.size()-1){
        remainder = '0' + remainder;
    }
    return remainder;

}
string CRC(string data,string gen="1101"){
    string st = data;
    for(int i=0;i<gen.size()-1;i++) data.push_back('0');
    string remainder = divideGetRemainder(data,gen);
    return st+remainder;
}

bool validateCRC(string encodedData,string gen="1011"){
    string remainder  = divideGetRemainder(encodedData,gen);
    if(stoi(remainder)==0) return true;
    else return false;
}

string decodeData(string encodedData,string gen="1011"){
    int dataLength = encodedData.size() - gen.size()+1;
    return encodedData.substr(0,dataLength);
}



int main(){

    srand(time(0));


    // Sender 

    string data;
    cout<<"Enter data: ";
    cin>>data;
    string code;
    cout<<"Enter the generator: ";
    cin>>code;
    string encodedData = CRC(data,code);
    
    cout<<"Data: "<<data<<endl;
    cout<<"CRC Encoded data: "<<encodedData;
    nl;

    // Sender




    // Noisy Channel

    double probCorruption = (rand()%101)/100.0;
    if(probCorruption>0.5){
        int randIndex = rand()%encodedData.size();
        encodedData[randIndex] = '0'+(1-(encodedData[randIndex]-'0'));

        cout<<"Corrupted data: "<<encodedData<<endl;
    }

    cout<<probCorruption<<endl;

    // Noisy Channel 


    


    // Receiver

    if(validateCRC(encodedData,code)){
        cout<<"No error found in the sent data\n";
        data = decodeData(encodedData,code);
        cout<<"Data is: "<<data<<endl;
    }
    else{
        cout<<"Corrupted data sent being detected\n";
    }

    // Receiver

return 0;
}