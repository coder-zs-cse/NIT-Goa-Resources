%{
    #include<stdlib.h>
    void yyerror(char*);
    #include "y.tab.h"
    

%}

alpha [A-Za-z]
digit [0-9]

%%

"if" return IF;
"else" return ELSE;
"print" return print;
{digit}+  {
    yylval = atoi(yytext);
    return NUM;
}
{alpha}({alpha}|{digit})* return ID;
. { return yytext[0];}

%%

int yywrap(){
    return 1;
}

