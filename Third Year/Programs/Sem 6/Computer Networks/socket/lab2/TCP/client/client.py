import socket

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

clientAddress = ('localhost',8000)

sock.connect(clientAddress)

try:
    filename = input("Enter the name of the file: ")
    print(f"Connection established to {clientAddress[0]}")
    sock.send(filename.encode())

    file = open(str(filename),"a")
    print("Receiving data")
    data = sock.recv(1024)
    while data:
        file.write(str(data.decode()))
        data = sock.recv(1024)
    print("File has opened")
    sock.close()
finally:
    sock.close()


print("Successfully got the file")
    