%{
    #include<stdio.h>
    #include "y.tab.h"
%}


%%

"int" {return INT;}
"main" {return MAIN;}
"void" {return VOID;}
"float" {return FLOATING;}
"printf" {return PRINTF;}
"return" {return RETURN;}

[(){}=,;+] {yylval = char(yytext[0]); return *yytext;}

[0-9]+.[0-9][0-9]* {yylval = atof(yytext); return FLOAT;}

[A-Za-z] {yylval = *yytext - 'a'; return ID;}

[0-9]+ {yylval = atoi(yytext); return NUMBER;}

\"[A-Za-z ]\" {yylval = *yytext; return STRING;}

[ \t] {}

. {yyerror();}

%% 

int yywrap(){
    printf("Donero");
    // return 1;
}
void yyerror(char *msg){
    printf("%s, invalid expression",msg);
}

int main(void){
    printf("Done");
    FILE *f = fopen("text.txt","r");
    yyin = f;
    yylex();
    f.close()
    return 0;
}