%{
    #include<stdio.h>
    void yyerror(char *s);
    int yylex(void);
    int sym[26];
%}

%token ID NUM;
%left '+' '-'

%%

line: statement ';' {printf("Parsed successfully");}
    | statement line {printf("Yes\n");}
    ;
statement: ID '=' expr { $$ = $3; sym[$1] = $3; }
    | expr { $$ = $1; printf("%d\n", $1); }
    ;
expr: term { $$ = $1;}
    | expr '+' expr { $$ = $1 + $3; }
    | expr '-' expr { $$ = $1 - $3; }
    | '(' expr ')' { $$ = $2; }
    ;
term: NUM 
    | ID { $$ = sym[$1]; }
%%
void yyerror(char *s) {
    printf("This is an error handler\n");
    fprintf(stderr, "%s\n", s);

}

int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}