%{
#include <stdio.h>
#include <stdlib.h>
#include "y.tab.h"
%}



%%
[ \t\n]         /* ignore whitespace */
if			{ return IF; }
else			{ return ELSE; }
"<="			{ return LE; }
">="			{ return GE; }
"=="			{ return EQ; }
"!="			{ return NE; }
"||"			{ return OR; }
"&&"			{ return AND; }

[0-9]+          		{ return NUM; }
[a-zA-Z][a-zA-Z0-9]* 	{ return ID; }
.               { return yytext[0]; }
%%

int yywrap() {
    return 1;
}

