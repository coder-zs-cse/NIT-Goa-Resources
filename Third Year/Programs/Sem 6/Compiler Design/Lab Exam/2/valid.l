%{
    #include<stdio.h>
    #include<string.h>
    void yyerror();
    int yywrap();
    int charCount = 0;
    int wordCount = 0;
    int blankCount =0;
    #define MAXENTRIES 1000
    typedef struct{
        char lexeme[50];
        char type[20];
        int value;
    }token;
    typedef struct{
        token entries[MAXENTRIES];
        int numTokens;
    }SymTable;
    SymTable symtable;
    void addToken(char*,char*,int);
%}

%%

"int"|"main"|"void"|"printf"|"return" { addToken(yytext, "Reserved", 0);charCount += yyleng;wordCount +=1;}
[a-z] {addToken(yytext,"Identifier",0);charCount +=1;wordCount+=1;}
[0-9]+ {addToken(yytext,"Constant",atoi(yytext));charCount+=1;}
[{}()=+;,] {addToken(yytext,"Symbol",0);}
[/][*].*[*][/] { charCount+=yyleng;}
"%d" {addToken(yytext,"Format Specifier",0);}
[ ] {blankCount++;}
[\n] {;}
. {}

%%

void yyerror(){
}
int yywrap(){
    return 1;
}

void addToken(char *name, char *type, int value){
    if(symtable.numTokens<MAXENTRIES){
        int i = 0;
        for(i=0;i<symtable.numTokens;i++){
            
            if(strcmp(symtable.entries[i].lexeme,name)==0){
                break;
            }
        }
        if(i==symtable.numTokens){
            // printf("New Entry: %s %s\n",name, type);
            strcpy(symtable.entries[i].lexeme,name);
            strcpy(symtable.entries[i].type,type);
            symtable.entries[i].value = value;
            symtable.numTokens++;
        }
    }
}

int main(int argc,int **argv){
    FILE *f = fopen("text.txt","r");
    yyin = f;
    yylex();
    printf("Number of characters: %d\n",charCount);
    printf("Number of words: %d\n",wordCount);
    printf("Number of blanks: %d\n",blankCount);
    printf("Symbol table\n");
    printf("Lexeme\tType\tValue\n");
    for(int i=0;i<symtable.numTokens;i++){
        printf("%s\t%s\t%d\n",symtable.entries[i].lexeme,symtable.entries[i].type,symtable.entries[i].value);
    }
    fclose(f);
    return 0;
}