%{
#include <stdio.h>
//#include "y.tab.h"
int yylex(void);
void yyerror(char *);
char addtotable(char,char,char);
void threeAdd();
int sym[26];
int index1=0;
char temp = 'A'-1;

struct expr{
char operand1;
char operand2;
char operator;
char result;
};
struct expr arr[20]; 
%}

%token VARIABLE INTEGER WHILE DO
%left '+' '-'
%left '*' '/'

%%
program:
program statement '\n'
|
;
statement:
VARIABLE '=' INTEGER';' {printf("\nA:= %d",$3);}
|WHILE {printf("\nL1:\n");}'(' condition ')' DO statement1 {printf("GOTO L1\nEND\n");}
;
condition: VARIABLE '>' INTEGER {printf("IF not A>%d GOTO END\n",$3);}
;
statement1: VARIABLE '=' expr {addtotable((char)$1,(char)$3,'=');threeAdd();}
;
expr: expr '+' expr  {$$ = addtotable((char)$1,(char)$3,'+');} 
| expr '-' expr  {$$ = addtotable((char)$1,(char)$3,'-');}
| expr '' expr  {$$ = addtotable((char)$1,(char)$3,'');}
| expr '/' expr  {$$ = addtotable((char)$1,(char)$3,'/');}
| '(' expr ')' { $$= (char)$2; }
|INTEGER {$$ = (char)$1;}
|VARIABLE {$$ = (char)$1+'a';}
;

%%

char addtotable(char a, char b, char o){
    temp++;
    arr[index1].operand1 = a;
    arr[index1].operand2 = b;
    arr[index1].operator = o;
    arr[index1].result=temp;
    index1++;
    return temp;
}

void threeAdd(){
    int i=0;
    char temp='A';
    while(i<index1){
        printf("%c:=\t",arr[i].result);
        printf("%c\t",arr[i].operand1);
        printf("%c\t",arr[i].operator);
        printf("%c\t",arr[i].operand2);
        i++;
        temp++;
        printf("\n");
    }
}

void yyerror(char *s) {
fprintf(stderr, "%s\n", s);
}
int main(void) {
//yyin=fopen("test.txt","r");
yyparse();
//threeAdd();
return 0;
}