%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int yylex();
void yyerror(char*);

int c = 0;
%}

%token NUM ID EQUALS GREATER LBRACE RBRACE ELSE IF PLUS MINUS SEMICOLON

%left PLUS MINUS
%nonassoc GREATER

%%

stmt_list : /* empty */
          | stmt_list stmt
          ;

stmt      : ID EQUALS expr SEMICOLON
          | IF LBRACE expr GREATER NUM RBRACE LBRACE stmt_list RBRACE ELSE LBRACE stmt_list RBRACE
          ;

expr      : expr PLUS term
          | expr MINUS term
          | term
          ;

term      : ID
          | NUM
          ;

%%

void yyerror(char* msg) {
    fprintf(stderr, "Error: %s\n", msg);
    exit(1);
}

int main() {
    yyparse();
    printf("c = %d\n", c);
    return 0;
}
