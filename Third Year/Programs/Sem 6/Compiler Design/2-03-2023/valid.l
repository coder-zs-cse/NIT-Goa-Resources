%{
    #include<stdlib.h>
    void yyerror(char*);
    #include "y.tab.h"
    extern yylval;
%}

alpha [A-Za-z]
digit [0-9]

%%

[0-9]+ {
    yylval = atoi(yytext);
    return INTEGER;
}
[-+\n] return *yytext;
[ \t] ;
. yyerror("invalid character");


%%
int yywrap(){
    return 1;
}