%{
#include<stdio.h>
int count=0;

struct node {
   char name[100];
   char type[100];
   struct node *next;
};

struct node* head=NULL;
struct node* Newnode(char n[],char t[]);
%}
%%
if|else|while|int|switch|for|char|scanf|printf {if(head==NULL)
{head=Newnode(yytext,"keyword");}
else{head->next=Newnode(yytext,"keyword");}}
[a-zA-Z][a-zA-Z0-9]* {if(head==NULL)
{head=Newnode(yytext,"identifier");}
else{head->next=Newnode(yytext,"identifier");}}
[0-9]* {if(head==NULL)
{head=Newnode(yytext,"number");}
else{head->next=Newnode(yytext,"number");}}
. {}
%%


struct node* Newnode(char n[],char t[])
{struct node* head=malloc(sizeof(node));
head->name=n;
head->type=t;
head->next=NULL;
return head;

}

void main()
{yyin=fopen("test.txt","r");
yylex();
struct node*p=head;
while(p!=NULL)
{
printf("%s:%s\n",p->name,p->type);


    p=p->next;
}


}