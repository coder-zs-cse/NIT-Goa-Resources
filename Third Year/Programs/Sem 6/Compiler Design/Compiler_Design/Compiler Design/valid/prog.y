%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
%}


%token ID
%token NUM

%%
start: statement ';' {printf("Instruction is correct and Parsed successfully\n");}
     | statement ';' start 
     ;
statement: ID '=' expr 
        | expr 
        ;

expr:term '+' term 
    | term '-' term
    | term '*' term
    | term '/' term
    | term 
    ;
term: ID 
    | NUM
    ;

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}
int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}