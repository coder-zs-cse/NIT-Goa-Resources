import socket
import time
import random 

SERVER_ADDRESS = ('127.0.0.1', 8000)

SEQ_NO = 0

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_socket.bind(SERVER_ADDRESS)

server_socket.listen(1)

print("Server listening...")

conn, addr = server_socket.accept()
print(f"Connected by {addr}")

while True:

    packets = conn.recv(1024).decode().split('#')[1:]

    for packet in packets:

        [packetSeqNo, data] = packet.split('|')
        packetLossProb = random.random().__round__(2)

        if(packetLossProb>0.5):
            print(f"---->Packet {packetSeqNo} is lost")
            conn.send(f'#'.encode())
            continue

        print(f'<Received packet: {packetSeqNo}')

        if(int(packetSeqNo)==SEQ_NO):
            ack = f'#ACK:{SEQ_NO}'
            conn.send(ack.encode())
            print(f"                                                >Sent {ack}")
            SEQ_NO+=1
        else:
            if(SEQ_NO!=0):
                ack = f'#ACK:{SEQ_NO-1}'
            else:
                ack = '#'
            conn.send(ack.encode())
            print(f"                                                >Sent {ack}")
            

# Close the connection
conn.close()
server_socket.close()