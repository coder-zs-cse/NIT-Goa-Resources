import socket
import random
import time

client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

serverAddress = ('localhost',8000)

client_socket.connect(serverAddress)

WINDOW_SIZE = 3
SEQ_NUM = 0

data = ['Packet 0', 'Packet 1', 'Packet 2', 'Packet 3', 'Packet 4', 'Packet 5', 'Packet 6', 'Packet 7', 'Packet 8', 'Packet 9', 'Packet 10']

inLoop = 0
TIME_OUT = 5
stime = time.time()

while SEQ_NUM < len(data):
    if(time.time()-stime>TIME_OUT):
        print("TimeOut occured, resending packets")
        inLoop = 0
    if inLoop==WINDOW_SIZE or inLoop+SEQ_NUM>=len(data):
        while(time.time()-stime<=TIME_OUT):
            pass
        print("TimeOut occured, resending packets")
        inLoop = 0
       
    if SEQ_NUM+inLoop < len(data):
        dataSent = '#' + str(SEQ_NUM+inLoop) + '|' + data[SEQ_NUM+inLoop]
        client_socket.send(dataSent.encode())
        print(f"                                                     >Sent: {data[SEQ_NUM+inLoop]}")
    inLoop+=1

    acks = client_socket.recv(1024).decode().split('#')
    
    for ack in acks:
        if(ack==''):
            continue
        ack_num = int(ack.split(':')[1])

        ackLossProb = random.random().__round__(2)
        if(ackLossProb>0.5):
            print(f"----->ACK {ack_num} is lost")
            continue

        if(ack_num>=SEQ_NUM):
            if(ack_num==SEQ_NUM):
                print(f"<Received ACK: {ack_num}")
            else:
                print(f'<Received Cumulative ACK: {ack_num}')
            
            SEQ_NUM = ack_num+1
            stime = time.time()
            inLoop=0
    
    
# Close the connection
client_socket.close()



